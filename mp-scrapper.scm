(use html-parser http-client sxml-modifications sxpath srfi-1 uri-common srfi-13 sxml-transforms matchable 
     fmt txpath regex utils)
(use sxml-serializer)

(include "guidebox.scm")

(define not-null? (o not null?))

(define (remove-crlf s)
  (string-substitute "\r\n" "" s #t))

(define enrich-episode (make-parameter enrich-from-guidebox))

(define open-url (make-parameter 
                   (lambda (url)
                     (call-with-input-request url #f (lambda (port)
                                                       (with-input-from-string (remove-crlf (read-all port))
                                                         (lambda ()
                                                           (html->sxml))))))))


(define (relative-uri from path)
  (uri->string (uri-relative-to (uri-reference path) (uri-reference from))))


(define-record state season episode season-episode)

(define (zero-pad width n)
  (pad-char #\0 (pad/left width (number->string n))))

(define (make-start-state)
  (make-state 1 1 1))

(define (next-season state)
  (make-state (add1 (state-season state))
              (state-episode state)
              1))

(define (next-episode state)
  (make-state (state-season state)
              (add1 (state-episode state))
              (add1 (state-season-episode state))))

(define (save state content)
  (let* ((season         (state-season state))
         (season-episode (state-season-episode state))
         (filename       (fmt #f "data/S" season "E" (zero-pad 2 season-episode)))
         (sxml-filename  (string-append filename ".scm"))
         (xml-filename   (string-append filename ".xml")))
    (with-output-to-file xml-filename (cut write-string (serialize-sxml content)))
    (with-output-to-file sxml-filename (cut write content))))

(define (format state summary script)
  (let* ((season         (state-season state))
         (episode        (state-episode state))
         (season-episode (state-season-episode state)))
    ((enrich-episode) state `(episode 
                               (@ (season ,season) 
                                  (episode ,episode) 
                                  (season-episode ,season-episode))
                               (summary ,@summary)
                               ,@script))))

(define (crawl-episode state url)
  (print "crawl " url)
  (let* ((doc     ((open-url) url))
         (summary ((sxpath `(// (center 2) // a *text*)) doc))
         (script  ((sxml-modify `("//body/table/tr"
                                  ,(sxml-rename 'rline))) doc))
         (script  ((txpath "//body/table/rline") script))
         (script  (pre-post-order* script
                                   `((rline *macro* . ,(lambda (t b)
                                                         (match b
                                                                ((attrs ('td . c1) ('td . c2) . _)
                                                                 (let ((character (string-join
                                                                                    (map string-trim-both
                                                                                         ((txpath "//text()") (cons 'td c1))))))
                                                                   (if (string=? "" character)
                                                                     `(line ,@c2)
                                                                     `(line (@ (character ,character)) ,@c2))))
                                                                ('()
                                                                 '()))))
                                     (font *macro* . ,(lambda (t b)
                                                        (match b
                                                               ((('@ ('id id)) . content)
                                                                `(dialog (@ (actor ,id))
                                                                         ,@content))
                                                               (x ;fallback
                                                                 '()))))
                                     (i *macro* . ,(lambda (t b)
                                                     `(scene ,@b)))
                                     (*text* . ,(lambda (trigger str) 
                                                  (let ((s (string-trim-both str)))
                                                    (if (string=? s "")
                                                      '()
                                                      s))))
                                     . ,alist-conv-rules*))))
    (save state (format state summary script))))

(define (crawl start-url)
  (let* ((doc ((open-url) start-url))
         (episodes_section ((car-sxpath `(// body table (tr 3) td)) doc)))
    (let loop ((state          (make-start-state))
               (season-started #f)
               (els            episodes_section))
      (if (not-null? els)
        (let* ((e             (car els))
               (episode-url   ((car-sxpath `(// a @ href *text*)) e)))
          (cond ((string? e)
                 (loop state season-started (cdr els)))
                ((null? episode-url)
                 (if season-started
                   (loop (next-season state) #f (cdr els))
                   (loop state #f (cdr els))))
                (else
                  (crawl-episode state (relative-uri start-url episode-url))
                  (loop (next-episode state) #t (cdr els)))))))))

(crawl "http://www.ibras.dk/montypython/mainlist.htm")
