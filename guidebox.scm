(use json vector-lib)

(define API_URL "http://api-public.guidebox.com/v1.43/USA")
(define API_KEY "<---- YOUR GUIDEBOX API KEY HERE ----->")
(define FLYING_CIRCUS_ID 2190)
(define EPISODE_ENDPOINT (fmt #f API_URL "/" API_KEY "/show/" FLYING_CIRCUS_ID "/episodes/"))

(define (json-ref json k #!key (eq equal?))
  (let ((i (vector-index (lambda (e) 
                           (eq (car e) k)) json)))
    (cdr (vector-ref json i))))

(define fetch-episode-info 
  (let* ((cache         '())
         (fetch-season! (lambda (season)
                          (let ((url  (fmt #f EPISODE_ENDPOINT season "/" 0 "/" 100)))
                            (call-with-input-request url #f (lambda (port)
                                                              (let* ((data (json-read port))
                                                                     (results (json-ref data "results")))
                                                                (pp results)
                                                                (set! cache (cons 
                                                                              (cons season results)
                                                                              cache))
                                                                results)))))))
    (lambda (season season-episode)
      (let* ((cached-season (assq season cache))
             (season-data (if cached-season
                            (cdr cached-season)
                            (fetch-season! season))))
        (find (lambda (e)
                (= (json-ref e "episode_number") season-episode)) season-data)))))

(define (enrich-from-guidebox state content)
  (let* ((season         (state-season state))
         (season-episode (state-season-episode state))
         (data           (fetch-episode-info season season-episode))
         (title          (json-ref data "title")))
    (match content
           (('episode ('@ . attrs) . b)
            `(episode ,(cons `(title , title) attrs)
                      ,@b)))))
