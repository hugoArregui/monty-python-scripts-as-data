# Monty Python Flying Circus scrapper (and data)

This script fetch information from [Monty Python Just The Words](http://www.ibras.dk/montypython/justthewords.htm) and [Guidebox](http://www.guidebox.com/)

## Data

You don't need to run the script to get the data, the files are already available in `data/` dir, both in glorious sxml format and in xml (if you are into that kind of stuff..).

## Running

If you want to run the scrapper you will need to:

- [Install chicken scheme](http://wiki.call-cc.org/)
- Set your [Guidebox](http://www.guidebox.com/) API key into `guidebox.scm`
- install the dependencies `chicken-install -s` in the root of this project
- `make run`
